#include <stdio.h>
void swap(int*a,int*b)
{
    *a=*a+*b;
    *b=*a-*b;
    *a=*a-*b;
}
int main()
{
    int a,b;
    printf("\nEnter the value of a");
    scanf("%d",&a);
    printf("\nEnter the value of b");
    scanf("%d",&b);
    swap(&a,&b);
    printf("Swapped value\n");
    printf("a=%d,b=%d",a,b);
    return 0;
}