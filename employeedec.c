#include<stdio.h>
int main()
{
    struct date
    {
     int d,m,y;
    };
    struct employee
    {
        char name[15];
        int emp_id[15];
        float salary;
        struct date doj;
    };
    struct employee data;
    printf("Enter Name");
    scanf("%s",data.name);
    printf("\n Enter Employee ID");
    scanf("%d",&data.emp_id);
    printf("\n Enter Salary");
    scanf("%f",&data.salary);
    printf("\n Enter Date Of Joining");
    scanf("%d%d%d",&data.doj.d,&data.doj.m,&data.doj.y);


    printf("\n Employee Details are: ");
    printf("Name:%s\n",data.name);
    printf("Employee ID:%d\n",data.emp_id);
    printf("Salary:%0.2f\n",data.salary);
    printf("DOJ:%d%d%d\n",data.doj.d,data.doj.m,data.doj.y);
    return 0;
}
